
config_dictionary={
  "paths": {
    "$": {
      "toEncrypt": {
          "main.request_body.encrypted_payload.encrypted_data": "main.request_body.encrypted_payload"
      },
      "toDecrypt": {
          "main.response_body.encrypted_payload": "main.response_body.encrypted_payload.encrypted_data"
      }
    }
  },
  "ivFieldName": "iv",
  "encryptedKeyFieldName": "encryptedKey",
  "encryptedValueFieldName": "encryptedData",
  "dataEncoding": "hex",
  "encryptionCertificate": "./path/to/public.cert", #nothing yet
  "decryptionKey": "./path/to/your/private.key", #nothing yet
  "oaepPaddingDigestAlgorithm": "SHA256"

}

