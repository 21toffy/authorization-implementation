request_body = {
  "message_type": "AUTH",
  "request_id": "rqst_A3E7-4FF6-C1C4-383F",
  "pos_transaction_status": "0",
  "processing_code": "280000",
  "obs_result": [
    {
      "service": "37",
      "result1": "V",
      "result2": " "
    }
  ],
  "transaction_amount": "1000",
  "transaction_currency_code": "840",
  "transaction_fee": "10",
  "settlement_amount": "15",
  "settlement_currency_code": "840",
  "cardholder_billing_amount": "15",
  "cardholder_billing_currency_code": "840",
  "cardholder_billing_conversion_rate": "70150000",
  "cardholder_billing_conversion_date": "1231",
  "transmission_date_and_time": "1231121515",
  "local_transaction_date_and_time": "1231064515",
  "merchant_category_code": "5943",
  "system_trace_audit_number": "107160",
  "retrieval_reference_number": "807318107160",
  "banknet_reference_number": "R1N1B1N19",
  "switch_serial_number": "123456789",
  "acquiring_institution": "000012345",
  "forwarding_institution": "9000001957",
  "transaction_category_code": "P",
  "card_acceptor_terminal": "TER12345",
  "card_acceptor_id": "MER123456789012",
  "card_acceptor_name": "RELIANCE MART",
  "card_acceptor_city": "PUNE",
  "card_acceptor_country": "IND",
  "unique_transaction_reference_number": "0555555901215305401",
  "additional_message": "0708457843120305A6008",
  "qr_data": "001006XX123400201012353611110030073041233004008LYL44354005009EEB9309570060070603***00001100terminal1044008Transfer009003add",
  "participant_id": "TERMINAL34728",
  "transaction_purpose": "08",
  "pos_pan_entry_mode": "03",
  "mastercard_assigned_id": "111111",
  "payment_facilitator_id": "00000123456",
  "payment_type": "C67",
  "advice_reason_code": "151",
  "advice_reason_detail_code": "0065",
  "transaction_replacement_amount": "1000",
  "settlement_replacement_amount": "15",
  "billing_replacement_amount": "15",
  "original": {
    "message_type_identifier": "0200",
    "system_trace_audit_number": "107701",
    "transmission_date_and_time": "1230184559",
    "acquiring_institution": "00000381167",
    "forwarding_institution": "00000270056"
  },
  "encrypted_payload": {
    "public_key_fingerprint": "4c4ead5927f0df8117f178eea9308daa58e27c2b",
    "encrypted_key": "A1B2C3D4E5F6112233445566",
    "oaep_hashing_algorithm": "SHA512",
    "iv": "df785a2fd9d058964d28506b64d8db50",
    "encrypted_data": {
      "primary_account_number": "5125589999999900",
      "receiver": {
        "first_name": "John",
        "middle_name": "T",
        "last_name": "Jones",
        "phone_number": "2147483647",
        "date_of_birth": "10162001",
        "account_number": "5125589999999900",
        "address": {
          "street": "123 Elm St",
          "city": "PUN",
          "state": "MAH",
          "country": "IND",
          "postal_code": "440016"
        },
        "identification": {
          "type": "00",
          "number": "B1122334455",
          "country_code": "IND",
          "expiry_date": "12212020",
          "nationality": "IND"
        },
        "country_of_birth": "IND"
      },
      "sender": {
        "first_name": "Simon",
        "middle_name": "T",
        "last_name": "Jones",
        "phone_number": "919886022666",
        "date_of_birth": "11201960",
        "account_number": "5222222222222220",
        "funding_source": "01",
        "sanctions_screening_score": "999",
        "digital_account_reference_number": "5111111111111111",
        "address": {
          "street": "123 Elm St",
          "city": "PUNE",
          "state": "MAH",
          "country": "IND",
          "postal_code": "440016"
        },
        "identification": {
          "type": "00",
          "number": "B2222334466",
          "country_code": "IND",
          "expiry_date": "10162001",
          "nationality": "IND"
        },
        "country_of_birth": "IND"
      }
    }
  }
}