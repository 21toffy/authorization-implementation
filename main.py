#would be needed to call an endpoint
# import requests

# imported as directed by the documentation to load up the configurations
from client_encryption.field_level_encryption_config import FieldLevelEncryptionConfig

# imported as directed by the documentation to encrypt and decryps payload/body
from client_encryption.field_level_encryption import encrypt_payload, decrypt_payload

# both imports are for payload data
from . request_body import request_body
from . response_body import response_body

#not important for now
# from config import config_dictionary

#dummy data for now ignore
# api_endpoint = 'https://xxx.com'
# response = requests.get(api_endpoint)


#class for encryption 
class Authorization_Mechanism_Encryption:
    def __init__(   self,
                    
                    request_body,
                    api_endpoint,
                    config,
                ):
        self.request_body = request_body
        
        # self.config = FieldLevelEncryptionConfig(config_dictionary)
        self.config = FieldLevelEncryptionConfig("./config.json")


    def request_body_data(self):
        return self.request_body

    #ethod to perform the encryption
    def encrypt_data(self):
        encrypted_request_payload = encrypt_payload(request_body, self.config)
        return encrypted_request_payload
        




class Authorization_Mechanism_Decryption:
    def __init__(   self,
                    response_body,
                    api_endpoint,
                    config,
                ):
        self.response_body = response_body

        # self.config = FieldLevelEncryptionConfig(config_dictionary)
        self.config = FieldLevelEncryptionConfig("./config.json")
        


    def response_body_data(self):
        return self.response_body

    #method to perform the decryption
    def decrypt_data(self):
        decrypted_request_payload = decrypt_payload(response_body, self.config)
        return decrypted_request_payload
        
